SELECT distinct dependent_ns.nspname
, dependent_view.relname
, source_ns.nspname 
, source_table.relname 

FROM pg_depend 
JOIN pg_rewrite ON pg_depend.objid = pg_rewrite.oid 
JOIN pg_class as dependent_view ON pg_rewrite.ev_class = dependent_view.oid 
JOIN pg_class as source_table ON pg_depend.refobjid = source_table.oid 

JOIN pg_namespace dependent_ns ON dependent_ns.oid = dependent_view.relnamespace
JOIN pg_namespace source_ns ON source_ns.oid = source_table.relnamespace

ORDER BY 1,2;


--Dependency : vw_fact_deposits_tvgdw it is a test change