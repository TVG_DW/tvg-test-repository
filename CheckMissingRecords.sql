--###########Morning load tables audit #################

SELECT * FROM public.tablerecordcount
WHERE tabletype IN ('Fact','Dimension','Reference')
AND recordcount <> sourcerecordcount
GO


----######Check missing records
--INSERT INTO public.applicants
SELECT t1.* 
FROM talend_staging.applicants_temp t1
LEFt JOIN public.applicants t2 ON t1.accountid = t2.accountid
WHERE t2.accountid IS NULL
GO

--INSERT INTO public.betresults
SELECt bht.* 
FROM talend_staging.BetResults_temp  bht(NOLOCK)
LEFT JOIN Betresults bh(NOLOCK) ON bh.serialnumber = bht.serialnumber
WHERE CAST(BHt.resulttimestamp AS DATE) = CAST(CONVERT_TIMEZONE('UTC','PDT',GETDATE()) AS DATE)
AND
bh.serialnumber IS NULL
ORDEr BY bht.resulttimestamp DESC
GO


--INSERT INTO public.bethistory
SELECt TOP 1000 bht.* 
FROM talend_staging.Bethistory_temp  bht(NOLOCK)
LEFT JOIN Bethistory bh(NOLOCK) ON bh.serialnumber = bht.serialnumber
WHERE CAST(BHt.transactiondate AS DATE) = CAST(CONVERT_TIMEZONE('UTC','PDT',GETDATE()) AS DATE)
AND
bh.serialnumber IS NULL
ORDEr BY bht.transactiondate DESC
Go


--INSERT INTO public.deposits
SELECt t1.* 
FROM talend_staging.deposits_temp t1(NOLOCK)
LEFT JOIN public.deposits t2(NOLOCK) ON t1.depositid = t2.depositid
WHERE t2.depositid IS NULL
GO

--INSERT INTO public.deposits
--DELETE FROM public.deposits
--WHERE depositid IN(
SELECt t1.depositid 
FROM public.deposits t1(NOLOCK)
LEFT JOIN talend_staging.deposits_temp t2(NOLOCK) ON t1.depositid = t2.depositid
WHERE t2.depositid IS NULL AND DATEPART(YEAR,t1.transactiondate) = 2016
AND DATEPART(MONTH,t1.transactiondate) = 9
--)
GO


--INSERT INTO public.eventdetail
SELECT t1.* 
FROM talend_staging.eventdetail_temp t1(NOLOCK)
LEFT JOIN public.eventdetail t2(NOLOCK) ON t1.eventpkid = t2.eventpkid
WHERE t2.eventpkid IS NULL
GO


--INSERT INTO public.event
SELECT *
FROM talend_staging.event_temp t1(NOLOCK)
LEFt JOIN public.event t2(NOLOCK) ON t1.eventpkid = t2.eventpkid
WHERE t2.eventpkid IS NULL
GO





--###################To calculate the delay###########################################
--####################################################################################
-----Betresults
SELECt TOP 1000 DATEDIFF(SECOND,talend_cdc_creation_date,warehouse_update_dt),talend_cdc_creation_date,warehouse_update_dt--,* 
FROM public.betresults(NOLOCK)
WHERE 
warehouse_update_dt IS NOT NULL 
AND 
CAST(talend_cdc_creation_date AS DATE) = CAST(CONVERT_TIMEZONE('UTC','PDT',GETDATE()) AS DATE)
--AND talend_cdc_creation_date BETWEEN '2016-10-08 13:48:00.000' AND '2016-10-08 15:00:00.000'
--AND DATEDIFF(MINUTE,talend_cdc_creation_date,warehouse_update_dt) >60
--ORDEr BY  DATEDIFF(SECOND,talend_cdc_creation_date,warehouse_update_dt) DESC
ORDEr BY talend_cdc_creation_date DESC
GO



-----Bethistory
SELECt TOP 1000  DATEDIFF(SECOND,talend_cdc_creation_date,warehouse_update_dt),talend_cdc_creation_date,warehouse_update_dt
FROM public.bethistory(NOLOCK)
WHERE 
warehouse_update_dt IS NOT NULL 
AND 
CAST(talend_cdc_creation_date AS DATE) = CAST(CONVERT_TIMEZONE('UTC','PDT',GETDATE()) AS DATE)
--talend_cdc_creation_date BETWEEN '2016-10-08 12:00:00.000' AND '2016-10-08 15:00:00.000'
ORDEr BY  DATEDIFF(SECOND,talend_cdc_creation_date,warehouse_update_dt) DESC
--ORDEr BY talend_cdc_creation_date DESC
GO


--Deposits
SELECt TOP 100  DATEDIFF(SECOND,talend_cdc_creation_date,warehouse_update_dt),talend_cdc_creation_date,warehouse_update_dt,talend_cdc_type
FROM public.deposits(NOLOCK)
WHERE 
warehouse_update_dt IS NOT NULL 
AND 
CAST(talend_cdc_creation_date AS DATE) = CAST(CONVERT_TIMEZONE('UTC','PDT',GETDATE()) AS DATE)
--talend_cdc_creation_date BETWEEN '2016-10-08 15:00:00.000' AND '2016-10-08 17:00:00.000'
--ORDEr BY  DATEDIFF(SECOND,talend_cdc_creation_date,warehouse_update_dt) DESC
ORDEr BY talend_cdc_creation_date DESC
GO


--applicants
SELECt  DATEDIFF(SECOND,talend_cdc_creation_date,warehouse_update_dt),talend_cdc_creation_date,warehouse_update_dt,talend_cdc_type
FROM public.applicants(NOLOCK)
WHERE 
warehouse_update_dt IS NOT NULL 
AND 
--CAST(talend_cdc_creation_date AS DATE) = CAST(CONVERT_TIMEZONE('UTC','PDT',GETDATE()) AS DATE)
talend_cdc_creation_date BETWEEN '2016-10-08 15:00:00.000' AND '2016-10-08 17:00:00.000'
--ORDEr BY  DATEDIFF(SECOND,talend_cdc_creation_date,warehouse_update_dt) DESC
--ORDEr BY talend_cdc_creation_date DESC
GO


--event
SELECt  DATEDIFF(SECOND,talend_cdc_creation_date,warehouse_update_dt),talend_cdc_creation_date,warehouse_update_dt,talend_cdc_type
FROM public.event(NOLOCK)
WHERE 
warehouse_update_dt IS NOT NULL 
AND 
--CAST(talend_cdc_creation_date AS DATE) = CAST(CONVERT_TIMEZONE('UTC','PDT',GETDATE()) AS DATE)
talend_cdc_creation_date BETWEEN '2016-10-08 15:00:00.000' AND '2016-10-08 17:00:00.000'
--ORDEr BY  DATEDIFF(SECOND,talend_cdc_creation_date,warehouse_update_dt) DESC
--ORDEr BY talend_cdc_creation_date DESC
GO



--eventdetail
SELECt  DATEDIFF(SECOND,talend_cdc_creation_date,warehouse_update_dt),talend_cdc_creation_date,warehouse_update_dt,talend_cdc_type
FROM public.eventdetail(NOLOCK)
WHERE 
warehouse_update_dt IS NOT NULL 
AND 
--CAST(talend_cdc_creation_date AS DATE) = CAST(CONVERT_TIMEZONE('UTC','PDT',GETDATE()) AS DATE)
talend_cdc_creation_date BETWEEN '2016-10-08 15:00:00.000' AND '2016-10-08 17:00:00.000'
--ORDEr BY  DATEDIFF(SECOND,talend_cdc_creation_date,warehouse_update_dt) DESC
--ORDEr BY talend_cdc_creation_date DESC
GO



--broadcastschedule
SELECt  DATEDIFF(SECOND,talend_cdc_creation_date,warehouse_update_dt),talend_cdc_creation_date,warehouse_update_dt,talend_cdc_type
FROM public.broadcastschedule(NOLOCK)
WHERE 
warehouse_update_dt IS NOT NULL 
AND 
--CAST(talend_cdc_creation_date AS DATE) = CAST(CONVERT_TIMEZONE('UTC','PDT',GETDATE()) AS DATE)
talend_cdc_creation_date BETWEEN '2016-10-08 15:00:00.000' AND '2016-10-08 17:00:00.000'
--ORDEr BY  DATEDIFF(SECOND,talend_cdc_creation_date,warehouse_update_dt) DESC
--ORDEr BY talend_cdc_creation_date DESC
GO






SELECt TOP 1000 
  EXTRACT(month from talend_cdc_creation_date) AS mth,
  EXTRACT(day from talend_cdc_creation_date) AS day,
  EXTRACT(hour from talend_cdc_creation_date) AS  hour,
  EXTRACT(minute from talend_cdc_creation_date) AS minute,
  COUNT(*) AS count,
  MIN(DATEDIFF(SECOND,talend_cdc_creation_date,warehouse_update_dt)) AS Min_delay,
  MAX(DATEDIFF(SECOND,talend_cdc_creation_date,warehouse_update_dt)) AS Max_delay

FROM public.betresults(NOLOCK)
WHERE 
warehouse_update_dt IS NOT NULL 
AND 
CAST(talend_cdc_creation_date AS DATE) = CAST(CONVERT_TIMEZONE('UTC','PDT',GETDATE()) AS DATE)
GROUP BY
EXTRACT(month from talend_cdc_creation_date),
EXTRACT(day from talend_cdc_creation_date) ,
EXTRACT(hour from talend_cdc_creation_date) ,
EXTRACT(minute from talend_cdc_creation_date)
ORDEr BY    EXTRACT(month from talend_cdc_creation_date),
          EXTRACT(day from talend_cdc_creation_date) ,
          EXTRACT(hour from talend_cdc_creation_date) ,
          EXTRACT(minute from talend_cdc_creation_date)